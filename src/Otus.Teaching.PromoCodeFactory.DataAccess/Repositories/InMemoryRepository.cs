﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T value)
        {
            return Task.Run(() => Data.Add(value));
        }

        public Task UpdateAsync(T value)
        {
            return Task.Run(() =>
            {
                T oldValue = Data.Where(x => x.Id == value.Id).FirstOrDefault();
                if (oldValue != null)
                {
                    Data.Remove(oldValue);
                }
                Data.Add(value);
            });
        }

        public Task DeleteAsync(Guid id)
        {
            return Task.Run(() => {
                T deleteValue = Data.Where(x => x.Id == id).FirstOrDefault();
                if (deleteValue != null)
                {
                    Data.Remove(deleteValue);
                }
            });
        }
    }
}